'use strict';

import * as express from 'express';
import {ClanController} from '../controller/ClanController';
import {CardController} from "../controller/CardController";

export const router = express.Router();

const clan = new ClanController();
const cards = new CardController();

router.route('/clan/:id').get(clan.getClan);
router.route('/clan/:id/warlog').get(clan.getWarLog);
router.route('/cards/:card').get(cards.getCard);
router.route('/cards/:card/url').get(cards.getCardUrl);