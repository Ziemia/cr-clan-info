const morgan = require('morgan');
import chalk from 'chalk';

const coloredStatusCode = status => {
    if(status >= 200 && status < 300) return chalk.hex("#1dbf3d").bold(status);
    else if(status >= 100 && status < 200 || status >= 300 && status < 400) return chalk.hex("#db730b").bold(status);
    else if(status >= 400 && status < 500) return chalk.hex("#c91029").bold(status);
};
export const morganLog = morgan(function (tokens, req, res){
    return [
        chalk.hex('#34ace0').bold("[CR Clan]"),
        chalk.hex('#34ace0').bold(tokens.method(req, res)),
        chalk.hex('#34ace0').bold(tokens.url(req, res)),
        chalk.hex('#34ace0').bold("-"),
        coloredStatusCode(tokens.status(req, res)),
        chalk.hex('#34ace0').bold("-"),
        chalk.hex('#34ace0').bold(tokens['response-time'](req, res) + ' ms'),
    ].join(' ');
});