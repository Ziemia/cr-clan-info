import {Request, Response} from "express";
import * as request from "request-promise-native";
import {ClanInfo} from "../entity/ClanInfo";
import {config} from "../config/environment";
import {Deserialize} from "cerialize";
import {logger} from "./logger";
import {WarLog} from "../entity/WarLog";
import {ClanWar} from "../entity/ClanWar";

export class ClanController {

    public static getBadge = (badgeId: number) => `http://localhost:${config.port}/cr-images/badges/${badgeId}.png`;

    public static parseDate = date => {
        const options = {
            day: "numeric",
            month: "numeric",
            year: "numeric",
            hour: "numeric",
            minute: "numeric"
        };
        const regex = /(^\d{4})(\d{2})(\d{2})(T)(\d{2})(\d{2})(\d{2})(.*Z)/;
        date = date.match(regex);
        return new Date(date[1] + "-" + date[2] + "-" + date[3] + date[4] + date[5] + ":" + date[6] + ":" + date[7] + ".000Z").toLocaleString('es-ES', options);
    };

    public async getClan(req: Request, res: Response){
        const clanId = req.params.id;
        try{
            const response = JSON.parse(await request.get({uri: `${config.api.url}clans/%23${clanId}`, auth: {bearer: config.api.token}}));
            const clan: ClanInfo = await Deserialize(response, ClanInfo);
            clan.badgeUrl = ClanController.getBadge(clan.badgeId);
            clan.memberList.forEach(member => {
                if(member.role === "leader") member.role = "Líder";
                else if(member.role === "coLeader") member.role = "Colíder";
                else if(member.role === "elder") member.role = "Veterano";
                else if(member.role === "member") member.role = "Miembro";
                member.lastSeen = ClanController.parseDate(member.lastSeen);
            });

            if(clan.location.name == "Spain") clan.location.name = "España";
            if(clan.type == "open") clan.type = "Abierto";
            else if(clan.type == "inviteOnly") clan.type= "Invitación";
            res.send(clan);
        } catch(e){
            logger.error("Clan Controller: " + e);
            if(e.name === "StatusCodeError"){
                const error = JSON.parse(e.error);
                if(error.reason === "notFound") res.render('error', {error: {status: "404", stack: `Clan not found with ID: ${clanId}`}, message: "Error"});
                else if(error.reason === "accessDenied.invalidIp") res.render('error', {error: {status: "403", stack: `Access denied for IP: ${e.stack.toString().match(/(?:[0-9]{1,3}\.){3}[0-9]{1,3}/)}`}, message: "Error"});
                else res.render('error', {error: e, message: "Error"});
            }
        }
    }

    public static getWarLogDataset = (log: WarLog, id) => new Promise(async (resolve, reject) => {
        const result = [];
        log.items.forEach(season => {
            // @ts-ignore
            const clan: ClanWar = season.standings.filter(s => s.clan.tag === `#${id}`)[0].clan;
            result.push({y: clan.clanScore, x: ClanController.parseDate(season.createdDate)})
        });

        resolve(result.reverse());
    });

    public async getWarLog(req: Request, res: Response){
        const clanId = req.params.id;
        try{
            const response = JSON.parse(await request.get({uri: `${config.api.url}clans/%23${clanId}/warlog`, auth: {bearer: config.api.token}}));
            const warLog: WarLog = await Deserialize(response, WarLog);
            warLog.dataset = await Deserialize(await ClanController.getWarLogDataset(warLog, clanId));
            res.send(warLog);
        } catch(e){
            logger.error("Clan Controller: " + e);
            if(e.name === "StatusCodeError"){
                const error = JSON.parse(e.error);
                if(error.reason === "notFound") res.render('error', {error: {status: "404", stack: `Clan not found with ID: ${clanId}`}, message: "Error"});
                else if(error.reason === "accessDenied.invalidIp") res.render('error', {error: {status: "403", stack: `Access denied for IP: ${e.stack.toString().match(/(?:[0-9]{1,3}\.){3}[0-9]{1,3}/)}`}, message: "Error"});
                else res.render('error', {error: e, message: "Error"});
            }
        }
    }

}