import {config} from "../config/environment";

export class CardController {

    public async getCardUrl(req, res){
        const cardName = req.params.card;
        const card = config.cards.cards.find(card => card.name.toLowerCase() === cardName.toLowerCase());

        if(card !== undefined) res.send(card.iconUrls.medium);
        else res.status(404).send("There's no card with name: " + cardName);
    }

    public async getCard(req, res){
        const cardName = req.params.card;
        const card = config.cards.cards.find(card => card.name.toLowerCase() === cardName.toLowerCase());

        if(card !== undefined) res.send(card);
        else res.status(404).send("There's no card with name: " + cardName);
    }
}