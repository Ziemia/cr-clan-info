import {IsBoolean, IsInt, IsString} from "class-validator";
import {deserialize} from "cerialize";

export class Location{

    @IsInt()
    @deserialize id: number;

    @IsString()
    @deserialize name: string;

    @IsBoolean()
    @deserialize isCountry: boolean;

    @IsString()
    @deserialize countryCode: string;
}