import {deserialize} from "cerialize";

export class DataPoint {
    @deserialize x: Date;

    @deserialize y: number;
}