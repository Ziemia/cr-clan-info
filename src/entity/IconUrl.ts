import {IsString} from "class-validator";
import {deserialize} from "cerialize";

export class IconUrl{

    @IsString()
    @deserialize medium: string;
}