import {IsInt, IsJSON, IsObject, IsString} from "class-validator";
import {deserialize, deserializeAs} from "cerialize";
import {Arena} from "./Arena";

export class ClanMember{

    @IsString()
    @deserialize tag: string;

    @IsString()
    @deserialize name: string;

    @IsString()
    @deserialize role: string;

    @IsString()
    @deserialize lastSeen: string;

    @IsInt()
    @deserialize expLevel: number;

    @IsInt()
    @deserialize trophies: number;

    @IsObject()
    @deserializeAs(Arena, 'arena') arena: Arena;

    @IsInt()
    @deserialize clanRank: number;

    @IsInt()
    @deserialize previousClanRank: number;

    @IsInt()
    @deserialize donations: number;

    @IsInt()
    @deserialize donationsReceived: number;

    @IsInt()
    @deserialize clanChestPoints: number;
}