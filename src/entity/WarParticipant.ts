import {IsInt, IsString} from "class-validator";
import {deserialize} from "cerialize";

export class WarParticipant{

    @IsString()
    @deserialize tag: string;

    @IsString()
    @deserialize name: string;

    @IsInt()
    @deserialize cardsEarned: number;

    @IsInt()
    @deserialize battlesPlayed: number;

    @IsInt()
    @deserialize wins: number;

    @IsInt()
    @deserialize collectionDayBattlesPlayed: number;

    @IsInt()
    @deserialize numberOfBattles: number;
}