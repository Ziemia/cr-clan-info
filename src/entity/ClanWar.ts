import {IsInt, IsString} from "class-validator";
import {deserialize} from "cerialize";

export class ClanWar{
    @IsString()
    @deserialize tag: string;

    @IsString()
    @deserialize name: string;

    @IsInt()
    @deserialize badgeId: number;

    @IsInt()
    @deserialize clanScore: number;

    @IsInt()
    @deserialize participants: number;

    @IsInt()
    @deserialize battlesPlayed: number;

    @IsInt()
    @deserialize wins: number;

    @IsInt()
    @deserialize crowns: number;
}