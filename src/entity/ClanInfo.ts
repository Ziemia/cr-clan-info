import {IsInt, IsObject, IsString, IsUrl} from "class-validator";
import {deserialize, deserializeAs} from "cerialize";
import {Location} from "./Location";
import {ClanMember} from "./ClanMember";

export class ClanInfo {

    @IsString()
    @deserialize tag: string;

    @IsString()
    @deserialize name: string;

    @IsString()
    @deserialize type: string;

    @IsString()
    @deserialize description: string;

    @IsInt()
    @deserialize badgeId: number;

    @IsUrl()
    @deserialize badgeUrl: string;

    @IsInt()
    @deserialize clanScore: number;

    @IsInt()
    @deserialize clanWarTrophies: number;

    @IsObject()
    @deserializeAs(Location, 'location') location: Location;

    @IsInt()
    @deserialize requiredTrophies: number;

    @IsInt()
    @deserialize donationsPerWeek: number;

    @IsString()
    @deserialize clanChestStatus: string;

    @IsInt()
    @deserialize clanChestLevel: number;

    @IsInt()
    @deserialize clanChestMaxLevel: number;

    @IsInt()
    @deserialize  members: number;

    @IsObject()
    @deserializeAs(ClanMember, 'memberList') memberList: Array<ClanMember>;

}
