import {deserializeAs} from "cerialize";
import {WarSeason} from "./WarSeason";
import {DataPoint} from "./DataPoint";


export class WarLog{
    @deserializeAs(WarSeason, 'items') items: Array<WarSeason>;

    @deserializeAs(DataPoint, 'dataset') dataset: Array<DataPoint>;
}