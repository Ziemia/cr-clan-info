import {deserializeAs} from "cerialize";
import {Card} from "./Card";

export class Cards {
    @deserializeAs(Card, 'items') cards: Array<Card>;
}