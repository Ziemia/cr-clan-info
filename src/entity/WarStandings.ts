import {deserialize, deserializeAs} from "cerialize";
import {ClanWar} from "./ClanWar";
import {IsInt} from "class-validator";

export class WarStandings{

    @deserializeAs(ClanWar, "clan") clan: ClanWar;

    @IsInt()
    @deserialize trophyChange: number;
}