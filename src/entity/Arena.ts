import {IsInt, IsString} from "class-validator";
import {deserialize} from "cerialize";

export class Arena{

    @IsInt()
    @deserialize id: number;

    @IsString()
    @deserialize name: string;
}