import {IsInt, IsString} from "class-validator";
import {deserialize, deserializeAs} from "cerialize";
import {WarParticipant} from "./WarParticipant";
import {WarStandings} from "./WarStandings";

export class WarSeason{

    @IsInt()
    @deserialize seasonId: number;

    @IsString()
    @deserialize createdDate: string;

    @deserializeAs(WarParticipant, 'participants') participants: Array<WarParticipant>;

    @deserializeAs(WarStandings, 'standings') standings: Array<WarStandings>;
}