import {IsInt, IsString} from "class-validator";
import {deserialize, deserializeAs} from "cerialize";
import {IconUrl} from "./IconUrl";

export class Card{

    @IsString()
    @deserialize name: string;

    @IsInt()
    @deserialize id: number;

    @IsInt()
    @deserialize maxLevel: number;

    @IsInt()
    @deserializeAs(IconUrl, 'iconUrls') iconUrls: IconUrl;
}