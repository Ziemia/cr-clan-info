import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import {logger} from "./controller/logger";
import {config} from "./config/environment";
import {morganLog} from "./controller/morganLog";
import * as path from "path";
import * as maintenance from 'maintenance';
import {router as apiRouter} from './routes/api';
// createConnection().then(async connection => {

const app = express();

app.set('view engine', 'pug');

app.use(morganLog);
app.use('/', express.static(__dirname + '/public'));
app.use("/home", (req, res) => res.sendFile(path.join(__dirname, '/public/home/index.html')));
app.use("/api", apiRouter);
// app.use("/", webRouter);
app.use("/cr-data", express.static(path.join(__dirname, 'public/cr-assets/data')));
app.use("/cr-images", express.static(path.join(__dirname, 'public/cr-assets/images')));
app.use(bodyParser.json());

app.listen(config.port);

maintenance(app, {
    current: false,
    httpEndpoint: true,
    url: "/maintenance",
    accessKey: "",
    api: "/api",
    status: 503,
    message: "Mantenimiento en curso, disculpe las molestias."
});

logger.info(`APP listening on port: ${config.port}`);

// }).catch(error => logger.error(error));
