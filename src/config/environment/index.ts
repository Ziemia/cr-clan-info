import * as request from "request-promise-native";
import {Deserialize} from "cerialize";
import {Cards} from "../../entity/Cards";


export const config = {
    port: 80,
    api: {
      url: "https://api.clashroyale.com/v1/",
      token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6IjAyNmUxNzhjLTAzNjEtNGUxMC05ZTgwLTBlZmY0OWU5ODNiYiIsImlhdCI6MTU5MTE2ODcxMCwic3ViIjoiZGV2ZWxvcGVyLzQ5YzJkZjhlLTM2MzYtZDdjMS1jYWZhLTg2N2ExZjI2MTA2MSIsInNjb3BlcyI6WyJyb3lhbGUiXSwibGltaXRzIjpbeyJ0aWVyIjoiZGV2ZWxvcGVyL3NpbHZlciIsInR5cGUiOiJ0aHJvdHRsaW5nIn0seyJjaWRycyI6WyI4Mi4yMjMuMTAzLjg3Il0sInR5cGUiOiJjbGllbnQifV19.MNY9zn2-JSe6-x7Z5hsFPD_vOUDMoeMBPlANitu2HNouNjfzKzyYBzYpv3IiAP2g1NY4mhMBZpAyYFbdxzjQlw"
    },
    cards: null
};

const getCards = async () => {
    const response = JSON.parse(await request.get({uri: `https://api.clashroyale.com/v1/cards`, auth: {bearer: config.api.token}}));
    config.cards = await Deserialize(response, Cards);
};

getCards();