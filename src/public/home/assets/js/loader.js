
const arenaIconFormatter = (value, date, cell, row, options) => `<img alt="Arena Icon" class="arena-img" src='../cr-images/arenas/${value._cell.row.data.arena.id}.png'>`;
const levelIconFormatter = (value, date, cell, row, options) => `<img alt="Level Icon" class="level-img" src='../cr-images/experience-levels/${value._cell.row.data.expLevel}.png'>`;
const nameDateFormatter = (value, date, cell, row, options) => `<div><div>${value._cell.row.data.name}</div><div class="last-seen">${value._cell.row.data.lastSeen}</div></div>`;
const battleLogIconFormatter = (value, date, cell, row, options) => `<button type="button" class="battle-log-btn" data-toggle="modal" data-target="#battle-log-modal"></button>`;
const donationsIconFormatter = (value, date, cell, row, options) => `<div><span>${value._cell.row.data.donations}</span><i style="margin-left: 5px; color: #2185d0" class="fa fa-arrow-up fa-lg donations"></i></div>`;
const receivedIconFormatter = (value, date, cell, row, options) => `<div><span>${value._cell.row.data.donationsReceived}</span><i style="margin-left: 5px; color: #f2711c" class="fa fa-arrow-down fa-lg donations"></i></div>`;

const replaceFavicon = badgeId => {
    let link = document.querySelector("link[rel*='icon']") || document.createElement('link');
    link.type = 'image/x-icon';
    link.rel = 'shortcut icon';
    link.href = `../cr-images/badges-icons/${badgeId}.ico`;
    document.getElementsByTagName('head')[0].appendChild(link);
};


const roleSorter = (a, b, aRow, bRow, column, dir, sorterParams) => {
    const relevance = new Map([
        ["Líder", 1],
        ["Colíder", 2],
        ["Veterano", 3],
        ["Miembro", 4]
    ]);

    const role1 = relevance.get(a);
    const role2 = relevance.get(b);
    return role1 - role2;
}

window.onload = () => {
    $.ajax({
        type: 'GET',
        url: "/api/clan/22Q8LURC",
        dataType: 'json',
        success: data => {
            window.document.title = data.name;
            replaceFavicon(data.badgeUrl.match(/^http:\/\/.*\/badges\/(.*).png/)[1]);
            document.getElementById("clan-name").innerHTML = data.name;
            document.getElementById("total-clan-trophies").innerHTML = data.clanScore;
            document.getElementById("total-clan-war-trophies").innerHTML = data.clanWarTrophies;
            document.getElementById("clan-tag").innerHTML = data.tag;
            document.getElementById("clan-description").innerHTML = data.description;
            document.getElementById("clan-badge").src = `/cr-images/badges/${data.badgeId}.png`;
            document.getElementById("clan-trophies").innerHTML = data.clanScore;
            document.getElementById("required-trophies").innerHTML = data.requiredTrophies;
            document.getElementById("weekly-donations").innerHTML = data.donationsPerWeek;
            document.getElementById("region").innerHTML = data.location.name;
            document.getElementById("type").innerHTML = data.type;
            document.getElementById("members").innerHTML = data.members + " / 50";

            let table = new Tabulator("#members-table", {
                height: "100%",
                data: data.memberList,
                layout: "fitData",
                responsiveLayout: "hide",
                pagination: "local",
                paginationSize: 10,
                movableColumns: false,
                resizableRows: false,
                initialSort: [
                    {column:"clanRank", dir:"asc"}
                ],
                columns:[
                    {title: "#", hozAlign: "center",vertAlign: "center", field:"clanRank", responsive: 7},
                    {title: "Nombre", vertAlign: "center", field:"name", cellClick:function(e, cell){console.log(cell.getRow().getData())}, formatter: nameDateFormatter, responsive: 0},
                    {title: "", vertAlign: "center", hozAlign: "center", formatter: battleLogIconFormatter, responsive: 6},
                    {title: "Nivel", vertAlign: "center", hozAlign: "center", field:"expLevel", formatter: levelIconFormatter, responsive: 4},
                    {title: "Trofeos", vertAlign: "center", hozAlign: "center", field:"trophies", responsive: 3},
                    {title: "Arena", vertAlign: "center", hozAlign: "center", field:"arena.id", formatter: arenaIconFormatter, responsive: 5},
                    {title: "Rol", vertAlign: "center", hozAlign: "center", field:"role", sorter: roleSorter, responsive: 2},
                    {title: "Donaciones", vertAlign: "center", hozAlign: "center", field:"donations", formatter: donationsIconFormatter, responsive: 0},
                    {title: "Recibidas", vertAlign: "center", hozAlign: "center", field:"donationsReceived", formatter: receivedIconFormatter, responsive: 1},
                ]
            })
        },
        error: err => console.log(err)
    });


    $.ajax({type: 'GET',
        url: "/api/clan/22Q8LURC/warlog",
        dataType: 'json',
        success: data => {
            const container = document.getElementById("clan-wars-chart");
            const x = [];
            const y = [];
            data.dataset.forEach(point => {
                x.push(point.x.split(" ")[0]);
                y.push(point.y);
            });
            let chart = new Chart(container, {
                type: 'line',
                data: {
                    labels: x,
                    datasets: [{
                        label: "Trofeos de Guerra",
                        fill: false,
                        borderColor: "rgba(254,141,10,0.6)",
                        backgroundColor: "white",
                        borderWidth: 4,
                        pointRadius: 3,
                        paintHoverRadius: 1,
                        // backgroundColor: "rgba(191, 63, 191, 0.8)",
                        data: y
                    },{
                        type: 'bar',
                        backgroundColor: "rgba(191, 63, 191, 0.9)",
                        hoverBackgroundColor: "rgba(63,192,191,1)",
                        label: "Trofeos de guerra",
                        data: y
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Trofeos'
                            }
                        }],
                        xAxes: [{
                            time: {
                                parser: "DD/MM/YYYY hh:mm",
                                unit: 'day'
                            },
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Fecha'
                            }
                        }]
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    }
                }

            });
        },
        error: e => console.log(e)
    });
};
